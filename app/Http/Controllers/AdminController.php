<?php

namespace App\Http\Controllers;

use App\Mail\SignupLink;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{
    public function sendLink()
    {
        $email = request()->validate([
            'email' => 'required',
        ]);
        $mailSend = Mail::to($email['email'])->send(new SignupLink());
        if($mailSend){
            return response('success');
        }e
    }
}
