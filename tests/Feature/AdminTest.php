<?php

namespace Tests\Feature;

use App\Mail\SignupLink;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class AdminTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_has_admin_rights()
    {
        $user = User::factory()->create(['user_role' => 'admin']);

        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => 'password',
        ]);

        $this->assertAuthenticated();
        $this->assertEquals($user->user_role, 'admin');
    }

    /** @test */
    public function admin_user_can_send_email_with_signup_link()
    {
        $this->withoutExceptionHandling();
        $user = User::factory()->create(['user_role'=> 'admin']);

        Mail::fake();

        $response = $this->actingAs($user,'sanctum')->post('/api/signup-link',[
            'email' => 'honey.kunhare@gmail.com'
        ]);

        Mail::assertSent(SignupLink::class, 1);

    }
}

